﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardVisualScript : MonoBehaviour {

	private float scale;
	private float deltaY;
	private bool orderInChildrenNeeded;

	private int indexOld = int.MinValue;
	private int indexInParentOld = int.MinValue;

	void Awake() {
		scale = GetComponent<CardParameters> ().scale;
		deltaY = GetComponent<CardParameters> ().deltaY;
		orderInChildrenNeeded = GetComponent<CardParameters> ().orderInChildrenNeeded;
	}

	public void OnPointerEnter() {
		GetComponent<RectTransform> ().anchoredPosition = new Vector2 (GetComponent<RectTransform> ().anchoredPosition.x, GetComponent<RectTransform> ().anchoredPosition.y + deltaY);
		transform.localScale = new Vector3 (scale, scale, 1.0f);

		if (orderInChildrenNeeded) {
			indexInParentOld = transform.parent.GetSiblingIndex ();
			transform.parent.SetAsLastSibling ();
		} 
			
		indexOld = transform.GetSiblingIndex ();
		transform.SetAsLastSibling ();

	}

	public void OnPointerExit() {
		GetComponent<RectTransform> ().anchoredPosition = new Vector2 (GetComponent<RectTransform> ().anchoredPosition.x, GetComponent<RectTransform> ().anchoredPosition.y - deltaY);
		transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);

		if (orderInChildrenNeeded) {
			if (indexInParentOld != int.MinValue)
				transform.parent.SetSiblingIndex (indexInParentOld);
		} 

		if (indexOld != int.MinValue)
			transform.SetSiblingIndex (indexOld);

	}
		
}
