﻿public enum CardType { ORDINARY_HERO, LEGENDARY_HERO, BOSS, CHAMBER_TRAPS, CHAMBER_MONSTERS }

public enum TreasureType { GOLD, BOOK, SWORD, ARTEFACT, OTHER }

public enum ChamberLevel { ORDINARY, ADVANCED }