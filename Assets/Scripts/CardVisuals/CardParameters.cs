﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CardParameters : MonoBehaviour {
	public float scale = 2.0f;
	public float deltaY = 0;

	public bool orderInChildrenNeeded;
}
