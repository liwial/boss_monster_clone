﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BossCardVisuals : MonoBehaviour {

	public TextMeshProUGUI captionMainText;
	public TextMeshProUGUI captionSmallerText;
	public TextMeshProUGUI levelUpText;
	public TextMeshProUGUI descriptionText;
	public TextMeshProUGUI pdText;

	public Image treasureImage;
	public Image cardImage;

}
