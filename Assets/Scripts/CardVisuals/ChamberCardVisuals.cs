﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChamberCardVisuals : MonoBehaviour {

	public TextMeshProUGUI captionMainText;
	public TextMeshProUGUI captionSmallerText;
	public TextMeshProUGUI descriptionText;
	public TextMeshProUGUI damageText;

	public Image cardImage;
	public Image[] treasureImages;

}
