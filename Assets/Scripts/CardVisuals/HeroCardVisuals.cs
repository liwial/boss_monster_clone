﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HeroCardVisuals : MonoBehaviour {

	public TextMeshProUGUI captionMainText;
	public TextMeshProUGUI captionSmallerText;
	public TextMeshProUGUI heroNameText;
	public TextMeshProUGUI descriptionText;
	public TextMeshProUGUI healthText;
	public TextMeshProUGUI damageText;

	public Image treasureImage;
	public Image cardImage;

	void Start () {
		
	}

	void Update () {
		
	}
}
