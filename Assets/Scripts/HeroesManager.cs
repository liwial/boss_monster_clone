﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroesManager : MonoBehaviour {

	public GameObject heroCardPrefab;

	private int heroCardAmountMax = 5;

	protected List<GameObject> cardList;
	public List<GameObject> CardList {
		get { return cardList; }
		set { cardList = value; }
	}

	void Awake() {

		cardList = new List<GameObject> ();

		for (int i = 0; i < heroCardAmountMax; i++) {
			GameObject cardObj = InstantiateCard (heroCardPrefab, "Hero Card " + (i + 1));
			cardList.Add (cardObj);
			cardObj.SetActive (false);
		}
	
	}

	private GameObject InstantiateCard (GameObject prefab, string name) {
		GameObject cardObj = Instantiate (prefab) as GameObject;
		cardObj.name = name;
		cardObj.transform.SetParent (this.transform);
		cardObj.transform.localScale = new Vector3 (1,1,1);

		cardObj.GetComponent<RectTransform> ().anchorMin = new Vector2(0.5f, 1f);
		cardObj.GetComponent<RectTransform> ().anchorMax = new Vector2(0.5f, 1f);
		cardObj.GetComponent<RectTransform> ().pivot = new Vector2(0.5f, 0.5f);

		return cardObj;
	}
		
	private void SetHero (GameObject card, ScriptableCardHero heroCard) {
		card.GetComponent<HeroCardVisuals> ().captionMainText.text = heroCard.captionMain;
		card.GetComponent<HeroCardVisuals> ().captionSmallerText.text = heroCard.captionSmaller;
		card.GetComponent<HeroCardVisuals> ().heroNameText.text = heroCard.heroName;
		card.GetComponent<HeroCardVisuals> ().descriptionText.text = heroCard.description;

		card.GetComponent<HeroCardVisuals> ().healthText.text = heroCard.health.ToString ();
		card.GetComponent<HeroCardVisuals> ().damageText.text = heroCard.damage.ToString ();

		card.GetComponent<HeroCardVisuals> ().treasureImage.sprite = heroCard.treasureSprite;
		card.GetComponent<HeroCardVisuals> ().cardImage.sprite = heroCard.cardImage;
	}

	public void AddCard (GameObject card, ScriptableCardHero heroCard, float spacing = 10.0f) {
		int activeCards = 0;
		foreach (GameObject c in cardList) {
			if (c.activeInHierarchy) {
				c.GetComponent<RectTransform> ().anchoredPosition = new Vector2 ( (c.GetComponent<RectTransform> ().anchoredPosition.x + (card.GetComponent<RectTransform> ().sizeDelta.x + spacing) ), c.GetComponent<RectTransform> ().anchoredPosition.y);
				activeCards++;
			}
		}

		SetHero (card, heroCard);
		card.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-400, -card.GetComponent<RectTransform> ().sizeDelta.y/2);
		//card.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (activeCards * (card.GetComponent<RectTransform> ().sizeDelta.x + spacing), card.GetComponent<RectTransform> ().anchoredPosition.y);
		card.SetActive (true);
	}
		
}
