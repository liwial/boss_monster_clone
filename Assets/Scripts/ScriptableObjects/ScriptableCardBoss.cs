﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Boss Card", menuName = "Cards/Boss")]
public class ScriptableCardBoss : ScriptableObject {

	public string captionMain;
	public string captionSmaller;
	public string description;

	public Sprite cardImage;
	public TreasureType treasureType;

	public int pd;


	[HideInInspector]
	public string captionLevelUp {
		get { return "Awans"; }
	}

	[HideInInspector]
	public CardType cardType {
		get { return CardType.BOSS; }
	}

	[HideInInspector]
	public Sprite treasureSprite {
		get { 
			if (treasureType == TreasureType.ARTEFACT)
				return (Sprite)Resources.Load("Graphics/Treasures/artifact", typeof(Sprite));
			else if (treasureType == TreasureType.BOOK)
				return (Sprite)Resources.Load("Graphics/Treasures/book", typeof(Sprite));
			else if (treasureType == TreasureType.GOLD)
				return (Sprite)Resources.Load("Graphics/Treasures/bag", typeof(Sprite));
			else if (treasureType == TreasureType.SWORD)
				return (Sprite)Resources.Load("Graphics/Treasures/sword", typeof(Sprite));
			else
				return (Sprite)Resources.Load("Graphics/Treasures/question", typeof(Sprite));
		}
	}
		
}