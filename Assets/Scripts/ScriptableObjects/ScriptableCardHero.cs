﻿//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Hero Card", menuName = "Cards/Hero")]
public class ScriptableCardHero : ScriptableObject {

	public CardType cardType;
	public TreasureType treasureType;
	public Sprite cardImage;

	public string captionMain;
	public string heroName;
	public string description;

	public int health;

	[HideInInspector]
	public string captionSmaller {
		get { 
			if (cardType == CardType.ORDINARY_HERO)
				return "Typowy bohater";
			else if (cardType == CardType.LEGENDARY_HERO)
				return "Legendarny bohater";
			else
				return string.Empty;
		}
	}

	[HideInInspector]
	public int souls {
		get { 
			if (cardType == CardType.ORDINARY_HERO)
				return 1;
			else if (cardType == CardType.LEGENDARY_HERO)
				return 2;
			else
				return 0;
		}
	}

	[HideInInspector]
	public int damage {
		get { 
			if (cardType == CardType.ORDINARY_HERO)
				return 1;
			else if (cardType == CardType.LEGENDARY_HERO)
				return 2;
			else
				return 0;
		}
	}

	[HideInInspector]
	public Sprite treasureSprite {
		get { 
			if (treasureType == TreasureType.ARTEFACT)
				return (Sprite)Resources.Load("Graphics/Treasures/artifact", typeof(Sprite));
			else if (treasureType == TreasureType.BOOK)
				return (Sprite)Resources.Load("Graphics/Treasures/book", typeof(Sprite));
			else if (treasureType == TreasureType.GOLD)
				return (Sprite)Resources.Load("Graphics/Treasures/bag", typeof(Sprite));
			else if (treasureType == TreasureType.SWORD)
				return (Sprite)Resources.Load("Graphics/Treasures/sword", typeof(Sprite));
			else
				return (Sprite)Resources.Load("Graphics/Treasures/question", typeof(Sprite));
		}
	}
}
