﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Chamber Card", menuName = "Cards/Chamber")]
public class ScriptableCardChamber : ScriptableObject {

	public CardType cardType;
	public ChamberLevel chamberLevel;

	public string captionMain;
	public string description;

	public Sprite cardImage;
	public TreasureType[] treasureTypes;

	public int damage;
	public int amount;

	[HideInInspector]
	public string captionSmaller {
		get { 
			if (cardType == CardType.CHAMBER_TRAPS && chamberLevel == ChamberLevel.ORDINARY)
				return "Komnata pułapek";
			else if (cardType == CardType.CHAMBER_TRAPS && chamberLevel == ChamberLevel.ADVANCED)
				return "Wypaśna komnata pułapek";
			else if (cardType == CardType.CHAMBER_MONSTERS && chamberLevel == ChamberLevel.ORDINARY)
				return "Komnata potworów";
			else if (cardType == CardType.CHAMBER_MONSTERS && chamberLevel == ChamberLevel.ADVANCED)
				return "Wypaśna komnata potworów";
			else
				return string.Empty;
		}
	}

	[HideInInspector]
	public Sprite[] treasureSprite {
		get {

			Sprite[] tmpSprite = new Sprite[treasureTypes.Length];

			for (int i = 0; i < treasureTypes.Length; i++) {

				if (treasureTypes[i] == TreasureType.ARTEFACT)
					tmpSprite[i] = (Sprite)Resources.Load ("Graphics/Treasures/artifact", typeof(Sprite));
				else if (treasureTypes[i] == TreasureType.BOOK)
					tmpSprite[i] = (Sprite)Resources.Load ("Graphics/Treasures/book", typeof(Sprite));
				else if (treasureTypes[i] == TreasureType.GOLD)
					tmpSprite[i] = (Sprite)Resources.Load ("Graphics/Treasures/bag", typeof(Sprite));
				else if (treasureTypes[i] == TreasureType.SWORD)
					tmpSprite[i] = (Sprite)Resources.Load ("Graphics/Treasures/sword", typeof(Sprite));
				else
					tmpSprite[i] = (Sprite)Resources.Load ("Graphics/Treasures/question", typeof(Sprite));

			}
			return tmpSprite;
		}
	}
		
}