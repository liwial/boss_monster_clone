﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPlacementManager : MonoBehaviour {

	public bool isPlayer;


	private int chamberCardAmountMax = 5;

	protected List<GameObject> chamberCardsList;
	public List<GameObject> ChamberCardsList {
		get { return chamberCardsList; }
		set { chamberCardsList = value; }
	}

	void Start() {

		chamberCardsList = new List<GameObject> ();

		for (int i = 0; i < chamberCardAmountMax; i++) {
			GameObject cardObj = InstantiateCard (GameplayManager.Instance.ChamberCardPrefab, "Chamber Card " + (i + 1));
			chamberCardsList.Add (cardObj);
			cardObj.SetActive (false);
		}

		//hero ... boss
		if (isPlayer) {
			GameObject cardObj = InstantiateCard (GameplayManager.Instance.HeroCardPrefab, "Hero Card");
			cardObj.transform.SetAsFirstSibling ();
			cardObj.SetActive (false);

			cardObj = InstantiateCard (GameplayManager.Instance.BossCardPrefab, "Boss Card");
			cardObj.transform.SetAsLastSibling ();
		} else { //boss ... hero
			GameObject cardObj = InstantiateCard (GameplayManager.Instance.HeroCardPrefab, "Hero Card");
			cardObj.transform.SetAsLastSibling ();
			cardObj.SetActive (false);

			cardObj = InstantiateCard (GameplayManager.Instance.BossCardPrefab, "Boss Card");
			cardObj.transform.SetAsFirstSibling ();
		}
			
	}

	private GameObject InstantiateCard (GameObject prefab, string name) {
		GameObject cardObj = Instantiate (prefab) as GameObject;
		cardObj.name = name;
		cardObj.transform.SetParent (this.transform);
		cardObj.transform.localScale = new Vector3 (1,1,1);

		return cardObj;
	}
		
}
