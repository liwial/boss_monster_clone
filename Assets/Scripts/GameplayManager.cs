﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameplayManager : MonoBehaviour {

	//public GameObject heroCardVisuals;

	public GameObject playerCardVisuals;
	public GameObject oponentCardVisuals;

	public GameObject playerHandVisualsPrefab;
	public GameObject oponentHandVisualsPrefab;

	public GameObject heroCardsPlacement;

	public GameObject heroCardPrefab;
	public GameObject HeroCardPrefab {
		get { return heroCardPrefab; }
	}

	public GameObject bossCardPrefab;
	public GameObject BossCardPrefab {
		get { return bossCardPrefab; }
	}

	public GameObject chamberCardPrefab;
	public GameObject ChamberCardPrefab {
		get { return chamberCardPrefab; }
	}
		
	//public Canvas canvas;
	//public Canvas Canvas {
	//	get { return canvas; }
	//	set { canvas = value; }
	//}

	private GameObject playerHandVisuals;
	private GameObject oponentHandVisuals;

	//private GameObject heroCardsVisuals;

	private List<ScriptableCardHero> heroCards;
	private List<ScriptableCardBoss> bossCards;
	private List<ScriptableCardChamber> chamberCards;

	//private int heroIndex = 0;
	private int handAmount = 5;
	private int heroesPerRound = 2;

	private static GameplayManager instance;
	public static GameplayManager Instance {
		get { return instance; }
	}


	void Awake() {

		if (instance != null && instance != this)
			Destroy (this.gameObject);
		else
			instance = this;

		playerHandVisuals = (GameObject)Instantiate (playerHandVisualsPrefab, transform.position, Quaternion.identity);
		SetPrefabParameters (playerHandVisuals, "HandPlayer", new Vector2 (0.5f, 0), new Vector2 (0.5f, 0), new Vector2 (0.5f, 0), new Vector3 (0, -70, 0));

		oponentHandVisuals = (GameObject)Instantiate (oponentHandVisualsPrefab, transform.position, Quaternion.identity);
		SetPrefabParameters (oponentHandVisuals, "HandOponent", new Vector2 (0.5f, 1), new Vector2 (0.5f, 1), new Vector2 (0.5f, 1), new Vector3 (0, 70, 0));

	}

	void Start () {
		//heroIndex = 0;
		//handAmount = 5;

		heroCards = PrepareHeroCards ();
		bossCards = PrepareBossCards ();
		chamberCards = PrepareChamberCards ();


		//ShowHeroCard(heroCards[heroIndex]);

		SetBoss (playerCardVisuals.transform.GetChild(playerCardVisuals.transform.childCount - 1).gameObject, bossCards[0]);
		SetBoss (oponentCardVisuals.transform.GetChild(0).gameObject, bossCards[1]);

		SetHand (playerHandVisuals);
		SetHand (oponentHandVisuals);

		//SetHeroes (heroCardsPlacement);
	}

	private void SetPrefabParameters(GameObject objectName, string name, Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot, Vector3 anchoredPosition) {
		objectName.SetActive (true);
		objectName.name = name;

		objectName.GetComponent<RectTransform> ().SetParent (GameObject.Find ("Canvas").transform, true);

		objectName.GetComponent<RectTransform> ().anchorMin = anchorMin;
		objectName.GetComponent<RectTransform> ().anchorMax = anchorMax;
		objectName.GetComponent<RectTransform> ().pivot = pivot;

		objectName.GetComponent<RectTransform> ().anchoredPosition = anchoredPosition;
		objectName.GetComponent<RectTransform> ().localScale = new Vector3 (1.0f, 1.0f, 1.0f);
	}

	private void SetHand(GameObject hand) {
		for (int i = 0; i < handAmount; i++) {
			hand.GetComponent<HandManager> ().AddCard (hand.GetComponent<HandManager> ().CardList [i], chamberCards [0]);
			chamberCards.RemoveAt (0);
		}
	}

	private void SetHeroes(GameObject heroes) {
		for (int i = 0; i < heroesPerRound; i++) {
			heroes.GetComponent<HeroesManager> ().AddCard (heroes.GetComponent<HeroesManager> ().CardList [i], heroCards [0]);
			heroCards.RemoveAt (0);
		}
	}

	private void SetBoss (GameObject bossVisuals, ScriptableCardBoss bossCard) {
		bossVisuals.GetComponent<BossCardVisuals> ().captionMainText.text = bossCard.captionMain;
		bossVisuals.GetComponent<BossCardVisuals> ().captionSmallerText.text = bossCard.captionSmaller;
		bossVisuals.GetComponent<BossCardVisuals> ().levelUpText.text = bossCard.captionLevelUp;
		bossVisuals.GetComponent<BossCardVisuals> ().descriptionText.text = bossCard.description;

		bossVisuals.GetComponent<BossCardVisuals> ().pdText.text = bossCard.pd.ToString ();

		bossVisuals.GetComponent<BossCardVisuals> ().treasureImage.sprite = bossCard.treasureSprite;
		bossVisuals.GetComponent<BossCardVisuals> ().cardImage.sprite = bossCard.cardImage;
	}

	/*
	private void ShowHeroCard (ScriptableCardHero heroCard) {
		if (heroIndex > -1 && heroIndex < heroCards.Count)
			SetVisualsHeroCard (heroCards [heroIndex]);
	}
	*/

	/*
	private void SetVisualsHeroCard (ScriptableCardHero heroCard) {
		heroCardVisuals.GetComponent<HeroCardVisuals> ().captionMainText.text = heroCard.captionMain;
		heroCardVisuals.GetComponent<HeroCardVisuals> ().captionSmallerText.text = heroCard.captionSmaller;
		heroCardVisuals.GetComponent<HeroCardVisuals> ().heroNameText.text = heroCard.heroName;
		heroCardVisuals.GetComponent<HeroCardVisuals> ().descriptionText.text = heroCard.description;

		heroCardVisuals.GetComponent<HeroCardVisuals> ().healthText.text = heroCard.health.ToString ();
		heroCardVisuals.GetComponent<HeroCardVisuals> ().damageText.text = heroCard.damage.ToString ();

		heroCardVisuals.GetComponent<HeroCardVisuals> ().treasureImage.sprite = heroCard.treasureSprite;
		heroCardVisuals.GetComponent<HeroCardVisuals> ().cardImage.sprite = heroCard.cardImage;
	}
	*/

	private List<ScriptableCardHero> PrepareHeroCards () {
		List<ScriptableCardHero> tmpOrdinaryList = new List<ScriptableCardHero> ();
		List<ScriptableCardHero> tmpLegendaryList = new List<ScriptableCardHero> ();

		Object[] heroCardsBegining = Resources.LoadAll ("Cards/HeroCards", typeof(ScriptableCardHero));
		ShuffleDeck (heroCardsBegining);

		foreach (Object card in heroCardsBegining) {
			ScriptableCardHero tmpCard = card as ScriptableCardHero;

			if (tmpCard.cardType == CardType.ORDINARY_HERO)
				tmpOrdinaryList.Add (tmpCard);
			else if (tmpCard.cardType == CardType.LEGENDARY_HERO)
				tmpLegendaryList.Add (tmpCard);

		}

		return tmpOrdinaryList.Union<ScriptableCardHero> (tmpLegendaryList).ToList<ScriptableCardHero> ();
	}

	private List<ScriptableCardBoss> PrepareBossCards () {
		List<ScriptableCardBoss> tmpList = new List<ScriptableCardBoss> ();

		Object[] bossCardsBegining = Resources.LoadAll ("Cards/BossCards", typeof(ScriptableCardBoss));
		ShuffleDeck (bossCardsBegining);

		foreach (Object card in bossCardsBegining) {
			ScriptableCardBoss tmpCard = card as ScriptableCardBoss;
			tmpList.Add (tmpCard);
		}

		return tmpList;
	}

	private List<ScriptableCardChamber> PrepareChamberCards () {
		List<ScriptableCardChamber> tmpList = new List<ScriptableCardChamber> ();

		Object[] chamberCardsBegining = Resources.LoadAll ("Cards/ChamberCards", typeof(ScriptableCardChamber));
		ShuffleDeck (chamberCardsBegining);

		foreach (Object card in chamberCardsBegining) {
			ScriptableCardChamber tmpCard = card as ScriptableCardChamber;
			tmpList.Add (tmpCard);
		}

		return tmpList;
	}

	private Object[] ShuffleDeck (Object[] deck) {
		if (deck != null) {
			for (int i = 0; i < deck.Length; i++) {
				Object tmp = deck [i];
				int randIndex = UnityEngine.Random.Range (i, deck.Length);
				deck [i] = deck [randIndex];
				deck [randIndex] = tmp;
			}

			return deck;
		}

		return null;
	}


	public void NextHeroCard() {
		//if (heroIndex < heroCards.Count - 1)
		//	heroIndex++;

		//ShowHeroCard(heroCards[heroIndex]);
	}

}
