﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandManager : MonoBehaviour {

	public GameObject chamberCardHandPrefab;


	private int cardAmountMax = 13;

	private List<GameObject> cardList;
	public List<GameObject> CardList {
		get { return cardList; }
		set { cardList = value; }
	}


	void Awake() {

		cardList = new List<GameObject> ();

		for (int i = 0; i < cardAmountMax; i++) {
			GameObject cardObj = Instantiate (chamberCardHandPrefab) as GameObject;
			cardObj.name = "Card " + (i + 1);
			cardObj.transform.SetParent (this.transform);
			cardObj.transform.localScale = new Vector3 (1,1,1);

			cardObj.GetComponent<RectTransform> ().anchorMin = new Vector2(0.5f, 0.5f);
			cardObj.GetComponent<RectTransform> ().anchorMax = new Vector2(0.5f, 0.5f);
			cardObj.GetComponent<RectTransform> ().pivot = new Vector2(0.5f, 1f);

			cardObj.SetActive (false);

			cardList.Add (cardObj);
		}

	}

	private void SetChamber (GameObject chamberVisuals, ScriptableCardChamber chamberCard) {
		chamberVisuals.GetComponent<ChamberCardVisuals> ().captionMainText.text = chamberCard.captionMain;
		chamberVisuals.GetComponent<ChamberCardVisuals> ().captionSmallerText.text = chamberCard.captionSmaller;
		chamberVisuals.GetComponent<ChamberCardVisuals> ().descriptionText.text = chamberCard.description;

		chamberVisuals.GetComponent<ChamberCardVisuals> ().damageText.text = chamberCard.damage.ToString ();

		chamberVisuals.GetComponent<ChamberCardVisuals> ().cardImage.sprite = chamberCard.cardImage;

		for (int i = 0; i < chamberCard.treasureTypes.Length; i++) {
			chamberVisuals.GetComponent<ChamberCardVisuals> ().treasureImages [i].sprite = chamberCard.treasureSprite [i];
			chamberVisuals.GetComponent<ChamberCardVisuals> ().treasureImages [i].gameObject.SetActive (true);
		}

		for (int i = chamberCard.treasureTypes.Length; i < 4; i++) {
			chamberVisuals.GetComponent<ChamberCardVisuals> ().treasureImages [i].gameObject.SetActive (false);
		}
	}

	public void AddCard (GameObject card, ScriptableCardChamber chamberCard, float spacing = 1.0f) {

		int activeCards = 0;

		foreach (GameObject c in cardList) {
			if (c.activeInHierarchy) {
				c.GetComponent<RectTransform> ().anchoredPosition = new Vector2 ( (c.GetComponent<RectTransform> ().anchoredPosition.x - (card.GetComponent<RectTransform> ().sizeDelta.x/2 + spacing) ), c.GetComponent<RectTransform> ().anchoredPosition.y);
				activeCards++;
			}
		}

		SetChamber (card, chamberCard);

		card.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (activeCards * (card.GetComponent<RectTransform> ().sizeDelta.x/2 + spacing), card.GetComponent<RectTransform> ().anchoredPosition.y);
		card.SetActive (true);
	}

}
