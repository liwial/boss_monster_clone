# Boss Monster Clone (work in progress)

Ready for now:

 - all cards of bosses, heroes and rooms
 
 - initial drawing cards for player's and opponent's hand
 
 - enlarging cards after mouse hovering

Project uses Unity 2017.3.0 and TextMesh Pro asset.

![alt text](Screenshots/bossMonster.png "Test scene")